﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentSupervisor.Migrations
{
    public partial class AddedManyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certification",
                columns: table => new
                {
                    CertificationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    DateAchieved = table.Column<DateTime>(nullable: false),
                    Field = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certification", x => x.CertificationID);
                });

            migrationBuilder.CreateTable(
                name: "ProfessorCertification",
                columns: table => new
                {
                    ProfessorID = table.Column<int>(nullable: false),
                    CertificationID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfessorCertification", x => new { x.ProfessorID, x.CertificationID });
                    table.ForeignKey(
                        name: "FK_ProfessorCertification_Certification_CertificationID",
                        column: x => x.CertificationID,
                        principalTable: "Certification",
                        principalColumn: "CertificationID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfessorCertification_professors_ProfessorID",
                        column: x => x.ProfessorID,
                        principalTable: "professors",
                        principalColumn: "ProfessorID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorCertification_CertificationID",
                table: "ProfessorCertification",
                column: "CertificationID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProfessorCertification");

            migrationBuilder.DropTable(
                name: "Certification");
        }
    }
}
