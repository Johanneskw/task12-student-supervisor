﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentSupervisor.Migrations
{
    public partial class AddedManyToManyUpdate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_students_professors_SupervisorProfessorID",
                table: "students");

            migrationBuilder.DropIndex(
                name: "IX_students_SupervisorProfessorID",
                table: "students");

            migrationBuilder.DropColumn(
                name: "SupervisorProfessorID",
                table: "students");

            migrationBuilder.AddColumn<int>(
                name: "ProfessorID",
                table: "students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_students_ProfessorID",
                table: "students",
                column: "ProfessorID");

            migrationBuilder.AddForeignKey(
                name: "FK_students_professors_ProfessorID",
                table: "students",
                column: "ProfessorID",
                principalTable: "professors",
                principalColumn: "ProfessorID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_students_professors_ProfessorID",
                table: "students");

            migrationBuilder.DropIndex(
                name: "IX_students_ProfessorID",
                table: "students");

            migrationBuilder.DropColumn(
                name: "ProfessorID",
                table: "students");

            migrationBuilder.AddColumn<int>(
                name: "SupervisorProfessorID",
                table: "students",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_students_SupervisorProfessorID",
                table: "students",
                column: "SupervisorProfessorID");

            migrationBuilder.AddForeignKey(
                name: "FK_students_professors_SupervisorProfessorID",
                table: "students",
                column: "SupervisorProfessorID",
                principalTable: "professors",
                principalColumn: "ProfessorID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
