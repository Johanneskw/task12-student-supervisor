﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentSupervisor.Migrations
{
    public partial class AddedManyToManyUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertification_Certification_CertificationID",
                table: "ProfessorCertification");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertification_professors_ProfessorID",
                table: "ProfessorCertification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorCertification",
                table: "ProfessorCertification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Certification",
                table: "Certification");

            migrationBuilder.RenameTable(
                name: "ProfessorCertification",
                newName: "professorCertifications");

            migrationBuilder.RenameTable(
                name: "Certification",
                newName: "certifications");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorCertification_CertificationID",
                table: "professorCertifications",
                newName: "IX_professorCertifications_CertificationID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_professorCertifications",
                table: "professorCertifications",
                columns: new[] { "ProfessorID", "CertificationID" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_certifications",
                table: "certifications",
                column: "CertificationID");

            migrationBuilder.AddForeignKey(
                name: "FK_professorCertifications_certifications_CertificationID",
                table: "professorCertifications",
                column: "CertificationID",
                principalTable: "certifications",
                principalColumn: "CertificationID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_professorCertifications_professors_ProfessorID",
                table: "professorCertifications",
                column: "ProfessorID",
                principalTable: "professors",
                principalColumn: "ProfessorID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_professorCertifications_certifications_CertificationID",
                table: "professorCertifications");

            migrationBuilder.DropForeignKey(
                name: "FK_professorCertifications_professors_ProfessorID",
                table: "professorCertifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_professorCertifications",
                table: "professorCertifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_certifications",
                table: "certifications");

            migrationBuilder.RenameTable(
                name: "professorCertifications",
                newName: "ProfessorCertification");

            migrationBuilder.RenameTable(
                name: "certifications",
                newName: "Certification");

            migrationBuilder.RenameIndex(
                name: "IX_professorCertifications_CertificationID",
                table: "ProfessorCertification",
                newName: "IX_ProfessorCertification_CertificationID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorCertification",
                table: "ProfessorCertification",
                columns: new[] { "ProfessorID", "CertificationID" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Certification",
                table: "Certification",
                column: "CertificationID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertification_Certification_CertificationID",
                table: "ProfessorCertification",
                column: "CertificationID",
                principalTable: "Certification",
                principalColumn: "CertificationID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertification_professors_ProfessorID",
                table: "ProfessorCertification",
                column: "ProfessorID",
                principalTable: "professors",
                principalColumn: "ProfessorID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
