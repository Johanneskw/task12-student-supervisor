﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor.Model
{
    class ProfessorCertification
    {
        //Many to many class, holds FKs for professors and certifications, to handle the many to many relation between professors and certificates. 
        public int ProfessorID { get; set; }
        public Professor professor { get; set; }
        public int CertificationID { get; set; }
        public Certification certification { get; set; }

    }
}
