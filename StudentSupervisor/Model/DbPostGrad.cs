﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor.Model
{
    class DbPostGrad : DbContext
    {
        //Setting up the DB schemas. 
        public DbSet<Professor> professors { get; set; }
        public DbSet<Student> students { get; set; }
        public DbSet<SpecializingProject> projects { get; set; }
        public DbSet<Certification> certifications { get; set; }
        public DbSet<ProfessorCertification> professorCertifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PC7385\SQLEXPRESS;Initial Catalog=PostGradDB;Integrated Security=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Setting key for one to one relation student-project. 
            modelBuilder.Entity<SpecializingProject>().HasKey(sp => new {sp.PrjectID});
            //Setting keys for many-many relation. joining class gets FKs to both sides of the relation. Creating a one to many relation between each side and the joiningclass. 
            //E.g. one instance og Professor can reference several professorCertifications and therefore many certificates, but the joiningclass can only reference one prfessor or certificate, to reference the correct one. 
            modelBuilder.Entity<ProfessorCertification>().HasKey(pc => new { pc.ProfessorID, pc.CertificationID });
            modelBuilder.Entity<ProfessorCertification>()
                .HasOne(p => p.professor)
                .WithMany(pc => pc.professorCertifications)
                .HasForeignKey(pid => pid.ProfessorID);
            modelBuilder.Entity<ProfessorCertification>()
                .HasOne(c => c.certification)
                .WithMany(pc => pc.professorCertifications)
                .HasForeignKey(cid => cid.CertificationID);
        }
    }
}
