﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor.Model
{
    class Certification
    {
        //Holds a list of professorCertificatins (a list of FKs to professors). 
        public int CertificationID { get; set; }
        public string Title { get; set; }
        public DateTime DateAchieved { get; set; }
        public string Field { get; set; }
        public List<ProfessorCertification> professorCertifications { get; set; }
    }
}
