﻿using StudentSupervisor.Model;
using System;

namespace StudentSupervisor
{
    //Class contains a GUI, wich takes commands and activates functions in the Db handler classes. 
    class Program
    {
        static void Main(string[] args)
        {
            GUI();
        }

        //Console GUI. Switch with methods based on userInput. when program loop is exited, professor-data is serialized to jsonFile in Bin.
        public static void GUI()
        {
            string input = "";
            while (input != "Q")
            {
                Console.WriteLine("\n\n***** Welcome to stundent supervisor handler *****");
                Console.WriteLine("Here are your options:\nPress 1 to insert new Student\nPress 2 to insert new professor\nPress 3 to update a student\nPress 4 to update a professor ");
                Console.WriteLine("Press 5 to delete a student\nPress 6 to delete a professor\nPress 7 to display all students\nPress 8 to display all professors\nPress Q to Quit");
                input = Console.ReadLine().ToUpper();
                try
                {
                    int res = Int32.Parse(input);
                    switch (res)
                    {
                        case 1:
                            StudentHandler.AddStudents();
                            break;
                        case 2:
                            ProfessorHandler.AddProfessors();
                            break;
                        case 3:
                            StudentHandler.UpdateStudent();
                            break;
                        case 4:
                            ProfessorHandler.UpdateProfessor();
                            break;
                        case 5:
                            StudentHandler.RemoveStudents();
                            break;
                        case 6:
                            ProfessorHandler.RemoveProfessors();
                            break;
                        case 7:
                            StudentHandler.DisplayStudents();
                            break;
                        case 8:
                            ProfessorHandler.DisplayProfessors();
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                ProfessorHandler.SerializeProfessors();
            }
        }
    }
}
