﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace StudentSupervisor.Model
{
    //DB handler class for professors. 
    static class ProfessorHandler
    {
        //Method to add a professor to db. Collects name and DOB from user, then gives option to create and give a certification.  . 
        public static void AddProfessors()
        {
            Console.WriteLine("Enter firstname: ");
            string firstname = Console.ReadLine();
            Console.WriteLine("Enter lastname: ");
            string lastname = Console.ReadLine();
            Console.WriteLine("Enter Date of birth: (yyyy, mm, dd)");
            DateTime dob = DateTime.Now;
            try
            {
                dob = DateTime.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Professor prof = new Professor() { Firstname = firstname, Lastname = lastname, DOB = dob };
            int pid = -1;
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                dbPostGrad.professors.Add(prof);
                dbPostGrad.SaveChanges();
                //Gets the auto incremented index of the created Prof. 
                pid = prof.ProfessorID;
            }
            Console.WriteLine("\nProfessor added, do you wish to add a certification?\nPress 1 to add Certificate\nPress anything to go to main menu");
            int num;
            if (Int32.TryParse(Console.ReadLine(), out num))
            {
                if (num == 1)
                {
                    //Creates a certification, and gets the ID, then assign the certificate to the created professor.
                    int certID = AddCertification();
                    AddProfessorCertification(pid, certID);
                    Console.WriteLine("Certification added to professor");
                }
            }
        }
        //Collects input and creates a certificate, then returns the ID to be used as FK in joiningtable. 
        public static int AddCertification()
        {
            Console.WriteLine("\nInsert certification title: ");
            string title = Console.ReadLine();
            Console.WriteLine("Insert field: ");
            string field = Console.ReadLine();
            Certification cert = new Certification() { Title = title, Field = field, DateAchieved = DateTime.Now };
            int certid = -1;
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                dbPostGrad.certifications.Add(cert);
                dbPostGrad.SaveChanges();
                certid = cert.CertificationID;
            }
            return certid;
        }
        //Takes FKs from professor and certificate to connect the two in joining-class. (Many to many relation).
        public static void AddProfessorCertification(int profID, int certID)
        {
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                dbPostGrad.professorCertifications.Add(new ProfessorCertification() { ProfessorID = profID, CertificationID = certID });
                dbPostGrad.SaveChanges();
            }
        }
        //Collects all prof.data to a list, then displays to console. Prof.ID used to access prof. trough console GUI. 
        public static void DisplayProfessors()
        {
            Console.WriteLine("\nProfessors: ");
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                List<Professor> profs = dbPostGrad.professors
                                                   .Include(p => p.Students)
                                                   .Include(pc => pc.professorCertifications)
                                                   .ThenInclude(c => c.certification).ToList<Professor>();
                foreach (Professor p in profs)
                {
                    List<ProfessorCertification> pcs = p.professorCertifications;
                    string certification = "";
                    if (pcs.Count == 0) certification = "Has no certifications";
                    foreach (ProfessorCertification pc in pcs)
                    {
                        certification += pc.certification.Title;
                    }
                    string stud = "";
                    if (p.Students.Count == 0) stud = "Has no students";
                    Console.WriteLine($"pid: {p.ProfessorID}. {p.Firstname} {p.Lastname}, DOB: {p.DOB.ToShortDateString()}, Certifications: {certification}, Students: {stud}");
                    foreach (Student s in p.Students)
                    {
                        Console.WriteLine($"{s.Firstname} {s.Lastname}");
                    }
                }
            }
        }

        //Allows user to change name of a prof. chooses prof by its ID. 
        public static void UpdateProfessor()
        {
            Console.WriteLine("\nInsert pid of professor to update: ");
            DisplayProfessors();
            int pid = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Insert new firstname: ");
            string firstname = Console.ReadLine();
            Console.WriteLine("Insert new lastname: ");
            string lastname = Console.ReadLine();
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                Professor prof = dbPostGrad.professors.Find(pid);
                Console.WriteLine($"current name is {prof.Firstname} {prof.Lastname}");
                prof.Firstname = firstname;
                prof.Lastname = lastname;
                Console.WriteLine($"new name is {prof.Firstname} {prof.Lastname}");
                dbPostGrad.SaveChanges();
            }
        }

        //Removes a professor by ID (Removing a professor also removes its students..)
        public static void RemoveProfessors()
        {
            Console.WriteLine("\nInsert pid of professor to be deleted: ");
            DisplayProfessors();
            int pid = Int32.Parse(Console.ReadLine());
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                Professor remove = dbPostGrad.professors.Find(pid);
                dbPostGrad.professors.Remove(remove);
                dbPostGrad.SaveChanges();
            }
            DisplayProfessors();
        }

        //Serializes all professorData to Json file ("Professors.json") in Bin.
        public static void SerializeProfessors()
        {
            using (DbPostGrad db = new DbPostGrad())
            {
                List<Professor> profs = db.professors
                                                   .Include(s => s.Students)
                                                   .ThenInclude(sp => sp.Project)
                                                   .Include(pc => pc.professorCertifications)
                                                   .ThenInclude(c => c.certification).ToList<Professor>();

                File.WriteAllText("Professors.json", JsonConvert.SerializeObject(profs, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }));
            }
        }

    }
}
