﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor.Model
{
    class Professor
    {
        //Holds a list of students (one to many) and a list of professorCertifications (joinig class for many to many relations). 
        public int ProfessorID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public List<Student> Students { get; set; }
        public List<ProfessorCertification> professorCertifications { get; set; }
    }
}
