﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor.Model
{
    class SpecializingProject
    {
        //Has FKs to Student (studentID) and a studentObject, holds the FK because table is dependant on student-table. 
        public int PrjectID { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public int StudentID { get; set; }
        public Student Student { get; set; }

    }
}
