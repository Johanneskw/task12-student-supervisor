﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentSupervisor.Model
{
    static class StudentHandler
    {
        //Same as addProfessors. Takes input, then creates a student. Also gives option to assing a supervisor and specializing project. 
        public static void AddStudents()
        {
            Console.WriteLine("Enter firstname: ");
            string firstname = Console.ReadLine();
            Console.WriteLine("Enter lastname: ");
            string lastname = Console.ReadLine();
            Console.WriteLine("Enter Date of birth: (yyyy, mm, dd)");
            DateTime dob = DateTime.Now;
            try
            {
                dob = DateTime.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Insert number of Professor to assign as supervisor: ");
            ProfessorHandler.DisplayProfessors();
            int pid = Int32.Parse(Console.ReadLine());
            Student student = new Student() { Firstname = firstname, Lastname = lastname, DOB = dob, ProfessorID = pid };
            int sid = -1;
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                dbPostGrad.students.Add(student);
                dbPostGrad.SaveChanges();
                sid = student.StudentID;
            }
            Console.WriteLine("\nStudent added, do you wish to add a project?\nPress 1 to add Project\nPress anything to go to main menu");
            int num;
            if (Int32.TryParse(Console.ReadLine(), out num))
            {
                if (num == 1)
                {
                    AddSpesializingProjects(sid);
                    Console.WriteLine("Project added to student");
                }
            }
        }
        //Creates a project, and then use the StudentID from the created Student to set as FK.
        public static void AddSpesializingProjects(int sid)
        {
            Console.WriteLine("\nProject Title: ");
            string title = Console.ReadLine();
            SpecializingProject project = new SpecializingProject() { Title = title, StartDate = DateTime.Now, StudentID = sid };
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                dbPostGrad.projects.Add(project);
                dbPostGrad.SaveChanges();
            }
        }

        //Collects all student w/ relevant fields to al List, then print all data to console. StudentID is used to access an object trough GUI. 
        public static void DisplayStudents()
        {
            Console.WriteLine("\nStudents:");
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                List<Student> students = dbPostGrad.students
                                                      .Include(s => s.Supervisor)
                                                      .Include(s => s.Project).ToList<Student>();
                foreach (Student s in students)
                {
                    string project = "no project";
                    if (s.Project != null) project = s.Project.Title;
                    Console.WriteLine($"sid: {s.StudentID}. {s.Firstname} {s.Lastname}, DOB: {s.DOB.ToShortDateString()}, Supervisor: {s.Supervisor.Firstname} {s.Supervisor.Lastname}, Project: {project}");
                }
            }
        }


        //Lets the user update eigther name or change supervisor of the student. 
        public static void UpdateStudent()
        {
            Console.WriteLine("\nInsert sid of Student to be updated: ");
            DisplayStudents();
            int sid = Int32.Parse(Console.ReadLine());
            Console.WriteLine("\nPress 1 to update Name\nPress 2 to update supervisor ");
            int res = Int32.Parse(Console.ReadLine());
            if (res == 1)
            {
                Console.WriteLine("\nEnter firstname: ");
                string firstname = Console.ReadLine();
                Console.WriteLine("Enter lastname: ");
                string lastname = Console.ReadLine();
                using (DbPostGrad dbPostGrad = new DbPostGrad())
                {
                    Student student = dbPostGrad.students.Find(sid);
                    Console.WriteLine($"current name is {student.Firstname} {student.Lastname}");
                    student.Firstname = firstname;
                    student.Lastname = lastname;
                    Console.WriteLine($"new name is {student.Firstname} {student.Lastname}");
                    dbPostGrad.SaveChanges();
                }
            }
            if (res == 2)
            {
                Console.WriteLine("\nInsert pid of professor to set as supervisor");
                ProfessorHandler.DisplayProfessors();
                int pid = Int32.Parse(Console.ReadLine());
                using (DbPostGrad dbPostGrad = new DbPostGrad())
                {
                    Student student = dbPostGrad.students.Find(sid);
                    student.ProfessorID = pid;
                    Console.WriteLine($"student got new supervisor");
                    dbPostGrad.SaveChanges();
                }
            }
        }


        //Removes a student based on ID;
        public static void RemoveStudents()
        {
            Console.WriteLine("\nInsert sid of Student to be deleted: ");
            DisplayStudents();
            int sid = Int32.Parse(Console.ReadLine());
            using (DbPostGrad dbPostGrad = new DbPostGrad())
            {
                Student remove = dbPostGrad.students.Find(sid);
                dbPostGrad.students.Remove(remove);
                dbPostGrad.SaveChanges();
            }
            DisplayStudents();
        }


    }
}
