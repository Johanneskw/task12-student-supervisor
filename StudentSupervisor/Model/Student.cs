﻿using StudentSupervisor.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudentSupervisor
{
    class Student
    {
        //References one prfessor (student can only have one supervisor), and one project (one to one). 
        public int StudentID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public int ProfessorID { get; set; }
        public Professor Supervisor { get; set; }
        public SpecializingProject Project { get; set; }

    }
}
